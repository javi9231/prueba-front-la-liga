# Prueba técnica React de LaLiga
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Versión de Node: 12.16.1 o superior
Versión de NPM: 6.13.4 o superiror
Compatibilidad: ES6 Navegadores evergreen (Chrome, Firefox, Edge, Safari)

## Instrucciones
- [Instrucciones](src/docs/laliga-prueba-tecnica-instrucciones.md)

## Entorno de desarrollo local

### `npm install`
Para instalación de dependencias

### `npm start`
Entorno de desarrollo

## Memoria
### Librerías
`redux-saga: 1.1.3`

`redux-devtools-extension: 2.13.8`

`eslint: 7.32.0`

`styled-components: 5.1.1`

`typescript: 3.7.2`

prettier": 2.1.0

## Descripción funcional

### `Pantalla de Login`

No se muestra si ya se tiene una sesión válida iniciada.

Se muestra al acceder a la web, si no se ha logeado antes el usuario. Hay que introducir el usuario (email) y el password correcto. Si no es así se muestra un mensaje de error.

Una vez introducidos los datos correctos la sesión persistirá hasta que se cierre la sesión.

### Pantalla Listado

No se podrá acceder si no hay una sesión válida iniciada.
Cargar el listado, por defecto la página 1 y muestra tras la carga un listado con 6 elementos.

Debajo del listado aparecen los accesos al número de páginas total para poder acceder.

En el caso de algún error se muestra un mensaje.
En la cabecera se muestra un botón que cierra la sesión y redirige a la pantalla de login.

## Descripción Técnica


### Login

Se han creado dos inputs uno de tipo email y otro del tipo password, dentro de un formulario. También se ha añadido un botón se submit con la leyenda “Inicio de sesión”. Para persistir los datos se ha usado Redux y Redux Saga para manejar los datos entre componentes, así como indicar que se ha establecido un login correcto. Si se recupera un token válido se persiste en el localStorage, por si se recarga la página o se cierra el navegador, este token se borra cuando se cierra la sesión.

### Listado

Al cargar llama al servicio y se trae la página 1 por defecto, la guarda en Redux y el listado la recupera, mostrando un listado con los datos. Debajo del listado se implementa una paginación para que el usuario pueda navegar por las páginas disponibles, viene indicado en el servicio el total de páginas.

## Flujo de la app

Al cargar se comprueba si existe un token en el localStorage, esto se hace mediante una acción de Redux, de esta forma se gestiona todo lo relacionado con el token en el localStorage. Si existe se inicia sesión. Si no es así te envía a la pantalla de login, con react-dom

### Login

Se comprueba por html5 que se ha introducido un email correcto y que existe una contraseña, cuando se pulsa sobre el botón de submit se comprueba que tenga el email y contraseña, entonces se envía una acción. Se llama al servicio y se espera respuesta, si es correcta se guarda el token en Redux y en localStorage, si no es así se lanza una acción de error y se muestra el error por pantalla. Al iniciar sesión se redirige a la pantalla de listado.

### Listado

Lanza una acción para recuperar la página 1 del servicio, si la recibe el listado está escuchando Redux, se hace un mapper de la información del servicio y la que se guarda en Redux. La paginación también recibe los datos y genera accesos para cada una de las páginas que existen en el servicio, se muestra debajo del listado.

Cuando se pulsa sobre una página que no es la que se muestra, se lanza otra acción para recuperar esa página.

### Cerrar Sesión

Si el usuario pulsa sobre Cerrar Sesión, se lanza otra acción para hacer logout, esto genera otras acciones primero borra el token del localstorage y después borra los datos de la store, los datos de inicio de sesión.

## Distribución de los ficheros


Todos se encuentran bajo la ruta src/
Los componentes se encuentran en la carpeta de components, actions en la carpeta common, en api los wrapers para evitar poner los headers en cada llamada POST, los reducers en otra del mismo nombre. Se han creado unos servicios para hacer las llamadas que están en services. En store los sagas, styles tiene los estilos de styled-components.




