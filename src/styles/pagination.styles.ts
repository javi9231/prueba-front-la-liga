import styled from 'styled-components';
import device from './device';

export const PaginationContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  @media screen and ${device.tablet} {
    width: 50%;
    margin: 0 auto;
  }
`;

export const PaginationItem = styled.a`
  width: 2em;
  border-radius: 2px;
  color: aliceblue;
`;
