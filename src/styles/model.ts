import styled, { keyframes } from 'styled-components';
import device from './device';

export const ContentContainer = styled.div`
  height: 100%;
  margin: 0;
  display: flex;
  flex-direction: column;

  @media screen and ${device.tablet} {
    height: 100vh;
    width: 100%;
    margin: 0;
  }
`;
export const HeaderContainer = styled.div`
  display: flex;
  position: inherit;
  width: 100%;
  justify-content: space-between;
  background-color: rgb(16, 32, 42);
`;
export const FooterContainer = styled.div`
  display: flex;
  position: inherit;
  max-width: 100%;
  justify-content: flex-end;
  background-color: rgb(16, 32, 42);
  color: aliceblue;
  padding: 1.5em;
`;

export const Title = styled.h1`
  text-align: left;
  font-size: 1.4em;
  font-weight: 700;
  margin-bottom: 16px;
  margin-left: 16px;
  color: aliceblue;
`;

export const LoginContainer = styled.div`
  max-width: 100%;
  height: 85vh;
  background: #25282a;
  display: flex;
  flex-direction: column;
  color: #fff;
  text-align: left;

  @media screen and ${device.tablet} {
    height: 100vh;
    width: 100%;
    margin: 0;
  }
`;
export const LoginContent = styled.div`
  max-width: 600px;
  margin: auto;
  padding: 30px;
  background: #202325;
`;

export const InputContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`;
export const Input = styled.input`
  width: 100%;
  max-width: fill-available;
  margin: 10px;
  float: left;
  color: #fff;
  padding: 10px;
  font-size: 16px;
  border: 2px solid #a9aeb4;
  background: rgba(0, 0, 0, 0);
  outline: none;
  box-sizing: border-box;
`;

export const Button = styled.button`
  width: 50%;
  height: 2em;
  text-align: center;
  background: #0077c8;
  border: none;
  color: #fff;
  transition: color 0.3s ease 0s, background 0.3s ease 0s,
    border-color 0.3s ease 0s;
  border-radius: 3px;
  text-transform: uppercase;
  line-height: 1.4;
`;
export const ErrorMSG = styled.div`
  width: 100%;
  text-align: center;
  padding: 1em;
  color: gold;
`;

const spinnerAnimation = keyframes`
 0% {
   transform: rotate(0deg);
 }
 100% {
   transform: rotate(360deg);
 }
`;

export const Spinner = styled.div`
  border: 4px solid rgba(255, 255, 255, 1);
  width: 36px;
  height: 36px;
  border-radius: 50%;
  border-left-color: transparent;
  animation: ${spinnerAnimation} 1s linear infinite;
`;

export const SpinnerContainer = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ListadoContainer = styled.div`
  width: 100%;
  height: 100%;
  margin: 0 auto;
  margin-top: 25px;
  color: white;

  @media screen and ${device.desktop} {
    width: 50%;
    margin: 0 5px;
  }
`;

export const StyledGrid = styled.div`
  width: 93%;
  margin: 0 auto;
  display: grid;
  height: auto;
  grid-auto-rows: minmax(20px, auto);
  grid-auto-flow: row;
  grid-template-columns: 1fr;
  gap: 10px;

  @media screen and ${device.tablet} {
    width: 50%;
    margin: 0 auto;
    grid-auto-flow: row;
    grid-template-columns: repeat(2, 1fr);
  }
`;

export const StyledCell = styled.div`
  height: 100%;
  min-width: 300px;
  grid-column-end: span 1;
  grid-row-end: span 1;
`;

export const UserContainer = styled.div`
  min-height: 130px;
  display: flex;
  border-radius: 2px;
  justify-content: flex-start;
  overflow: hidden;
  background-color: rgb(16, 32, 42);
  position: relative;
`;

export const AvatarContainer = styled.div`
  min-height: 130px;
  -webkit-transition: all 1s ease; /* Safari and Chrome */
  -moz-transition: all 1s ease; /* Firefox */
  -ms-transition: all 1s ease; /* IE 9 */
  -o-transition: all 1s ease; /* Opera */
  transition: all 1s ease;

  &:hover {
    -webkit-transform: scale(1.25); /* Safari and Chrome */
    -moz-transform: scale(1.25); /* Firefox */
    -ms-transform: scale(1.25); /* IE 9 */
    -o-transform: scale(1.25); /* Opera */
    transform: scale(1.25);
  }
`;
export const DataContainer = styled.div`
  flex-direction: column;
  margin: 10px;
`;
