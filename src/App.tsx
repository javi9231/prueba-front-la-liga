import React, { useEffect } from 'react';
import { Route, Switch, Redirect } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import Footer from './components/footer';
import { ContentContainer, HeaderContainer } from './styles/model';
import UsersComponent from './components/users';
import { IRootState } from './reducers';
import Login from './components/login';
import Header from './components/header';
import { isTokenInLocalStorage } from './actions';
import GlobalStyle from './styles/globalStyle';

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(isTokenInLocalStorage());
  }, []);
  const isLogin =
    useSelector((state: IRootState) => state.auth.isLogin) || false;

  const handleUserrender = () =>
    isLogin ? <UsersComponent /> : <Redirect to="/login" />;
  return (
    <>
      <ContentContainer>
        <GlobalStyle />
        <HeaderContainer>
          <Header />
        </HeaderContainer>
        <BrowserRouter>
          <Switch>
            <Route exact path="/">
              <Redirect to="/users" />
            </Route>
            <Route path="/users" render={handleUserrender} />
            <Route
              path="/login"
              render={() => (!isLogin ? <Login /> : <Redirect to="/users" />)}
            />
          </Switch>
        </BrowserRouter>
        <Footer />
      </ContentContainer>
    </>
  );
};

export default App;
