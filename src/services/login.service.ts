import { postWrapper } from '../api';

const URL = 'https://reqres.in/api/login';

export interface BaseLoginCall {
  email: string;
  password: string;
}

export const authorize = async ({
  email,
  password,
}: BaseLoginCall): Promise<any> => {
  const requestBody = { body: JSON.stringify({ email, password }) };

  const resultado = await postWrapper(URL, requestBody)
    .then((res) => res.json())
    .catch((err) => err);
  return resultado;
};

export const saveTokenAuth = ({ token }) => {
  localStorage.setItem('token', token);
};

export const readTokenAuth = () => localStorage.getItem('token');

export const deleteTokenAuth = () => localStorage.removeItem('token');
