import { BaseAction, actionIds } from '../common';

export const userListRequestAction = (pageNumber: number): BaseAction => ({
  type: actionIds.USER_LIST_REQUEST,
  payload: pageNumber,
});

export const userListSuccessAction = (data): BaseAction => ({
  type: actionIds.USER_LIST_SUCCESS,
  payload: data,
});

export const userListErrorAction = (error): BaseAction => ({
  type: actionIds.USER_LIST_ERROR,
  payload: error,
});
