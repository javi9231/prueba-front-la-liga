import { BaseAction, actionIds } from '../common';
import { BaseLoginCall } from '../services';

export const loginRequestInAction = ({
  email,
  password,
}: BaseLoginCall): BaseAction => ({
  type: actionIds.LOGIN_REQUEST_IN,
  payload: { email, password },
});

interface BaseTokenReturn {
  token: string;
}
export const loginRequestCompletedAction = ({
  token,
}: BaseTokenReturn): BaseAction => ({
  type: actionIds.LOGIN_REQUEST_SUCCESS,
  payload: token,
});

interface BaseTokenError {
  error: string;
}

export const loginRequestErrorAction = ({
  error,
}: BaseTokenError): BaseAction => ({
  type: actionIds.LOGIN_REQUEST_ERROR,
  payload: error,
});

export const loginoutRequestAction = (): BaseAction => ({
  type: actionIds.LOGOUT_REQUEST,
  payload: null,
});

export const isTokenInLocalStorage = (): BaseAction => ({
  type: actionIds.RECOVER_TOKEN_FROM_LOCALSTORAGE,
  payload: null,
});
