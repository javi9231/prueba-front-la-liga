import { fork, call, take, put } from 'redux-saga/effects';
import {
  authorize,
  saveTokenAuth,
  deleteTokenAuth,
  readTokenAuth,
} from '../services';
import { actionIds } from '../common';

function* requestToLoginIn({ email, password }) {
  try {
    const token = yield call(authorize, { email, password });
    // El servicio acepta cualquier password
    // Se obliga a que el password sea el de la historia de usuario cityslicka
    if (token.token && password === 'cityslicka') {
      yield call(saveTokenAuth, token);
      yield put({
        type: actionIds.LOGIN_REQUEST_SUCCESS,
        payload: token,
      });
    } else {
      throw new Error('Error');
    }
  } catch (error) {
    yield put({ type: actionIds.LOGIN_REQUEST_ERROR, payload: error });
  }
}

export function* loginFlow() {
  while (true) {
    const userData = yield take([
      actionIds.LOGIN_REQUEST_IN,
      actionIds.LOGOUT_REQUEST,
    ]);
    if (userData.payload) {
      yield fork(requestToLoginIn, userData.payload);
      yield take([actionIds.LOGOUT_REQUEST, actionIds.LOGIN_REQUEST_ERROR]);
    }
    yield call(deleteTokenAuth);
    yield put({ type: actionIds.LOGOUT_REQUEST });
  }
}
export function* wathTokenRecover() {
  yield take(actionIds.RECOVER_TOKEN_FROM_LOCALSTORAGE);
  const token = yield call(readTokenAuth);
  if (token) {
    yield put({
      type: actionIds.LOGIN_REQUEST_SUCCESS,
      payload: token,
    });
  }
}
