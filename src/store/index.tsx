import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { all, fork } from 'redux-saga/effects';
import { rootReducers } from '../reducers';
import { loginFlow, wathTokenRecover } from './login.sagas';
import wathgetUserList from './users.sagas';

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
  rootReducers,
  composeWithDevTools(applyMiddleware(sagaMiddleware)),
);
function* rootSaga() {
  yield all([fork(loginFlow), fork(wathTokenRecover), fork(wathgetUserList)]);
}
sagaMiddleware.run(rootSaga);

export default store;
