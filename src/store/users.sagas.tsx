import { take, put, call, fork, delay } from 'redux-saga/effects';
import { fetchUserList } from '../services';
import { actionIds } from '../common';

function* getUserList(pageNumber) {
  try {
    const result = yield call(fetchUserList, pageNumber.payload);
    // se mete un retraso de 3 seg para que se vea el spinner
    yield delay(3000);
    if (result) {
      yield put({
        type: actionIds.USER_LIST_SUCCESS,
        payload: result,
      });
    }
  } catch (error) {
    yield put({ type: actionIds.USER_LIST_ERROR, payload: error });
  }
}
function* wathgetUserList() {
  while (true) {
    const pageNumber = yield take(actionIds.USER_LIST_REQUEST);
    yield fork(getUserList, pageNumber);
  }
}
export default wathgetUserList;
