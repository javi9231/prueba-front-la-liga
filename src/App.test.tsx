import React from "react";
import { render } from './common/test.utils';
import App from "./App";

test("renders learn react link", () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Prueba técnica de LaLiga/i);
  expect(linkElement).toBeInTheDocument();
});
