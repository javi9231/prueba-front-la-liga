export const actionIds = {
  LOGIN_REQUEST_IN: '[0] Request a login',
  LOGIN_REQUEST_SUCCESS: '[1] Get login Token async service returned a token',
  LOGIN_REQUEST_ERROR: '[2] User/Password incorrect',
  LOGOUT_REQUEST: '[3] Request login out',
  RECOVER_TOKEN_FROM_LOCALSTORAGE: '[4] Recover token if is in localstorage',
  USER_LIST_REQUEST: '[5] Request user list',
  USER_LIST_SUCCESS: '[6] List of users has been received',
  USER_LIST_ERROR: '[7] Error: List of users has not been received',
};

export interface BaseAction {
  type: string;
  payload: any;
}
