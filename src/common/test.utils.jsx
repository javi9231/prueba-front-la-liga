import * as React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import { store } from '../store';

function ReduxProvider({ children }) {
  return <Provider store={store}>{children}</Provider>;
}

const reduxRender = (ui, options) =>
  render(ui, { wrapper: ReduxProvider, ...options });

export * from '@testing-library/react';

export { reduxRender as render };

ReduxProvider.propTypes = {
  children: PropTypes.any,
};
