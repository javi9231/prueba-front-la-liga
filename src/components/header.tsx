import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Title } from '../styles/model';
import { loginoutRequestAction } from '../actions';
import { IRootState } from '../reducers';

const Header = () => {
  const dispatch = useDispatch();
  const isLogin =
    useSelector((state: IRootState) => state.auth.isLogin) || false;

  const handleLogout = () => dispatch(loginoutRequestAction());
  return (
    <>
      <Title>Prueba técnica de LaLiga</Title>
      {isLogin ? (
        <button type="button" name="logout" onClick={handleLogout}>
          Cerrar Sesión
        </button>
      ) : null}
    </>
  );
};

export default Header;
