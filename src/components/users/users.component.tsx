import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  ErrorMSG,
  ListadoContainer,
  Spinner,
  SpinnerContainer,
} from '../../styles/model';
import Listado from './listado.component';
import { IRootState } from '../../reducers';
import Pagination from './pagination.component';
import ERROR from '../../common/errors.const';

const Users = () => {
  const [error, setError] = useState<string>();

  const isLoading =
    useSelector((state: IRootState) => state.users.isLoading) || false;
  const pageData = useSelector((state: IRootState) => state.users.data) || {};
  const usersError =
    useSelector((state: IRootState) => state.users.error) || '';

  useEffect(() => {
    if (usersError) {
      setError(ERROR.USERS_UNABLE_TO_RETRIVE_LIST);
    }
  }, [usersError]);

  return (
    <>
      {isLoading && (
        <SpinnerContainer>
          <Spinner />
        </SpinnerContainer>
      )}
      <>
        {pageData.page && !isLoading && (
          <ListadoContainer>
            <Listado pageData={pageData} />
          </ListadoContainer>
        )}
        <Pagination
          numPages={pageData.totalPages || 1}
          actualPage={pageData.page}
        />
      </>
      {error && <ErrorMSG />}
    </>
  );
};

export default Users;
