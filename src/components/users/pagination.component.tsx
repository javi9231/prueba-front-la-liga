import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { userListRequestAction } from '../../actions';
import {
  PaginationContainer,
  PaginationItem,
} from '../../styles/pagination.styles';

interface Param {
  numPages: number;
  actualPage: number;
}
const Pagination = ({ numPages, actualPage }: Param) => {
  const dispatch = useDispatch();
  const [pageNumberSelect, setPageNumberSelect] = useState<number>(1);
  const [blocks, setBlocks] = useState<number[]>([]);

  const generatePaginationNumbers = (num) => {
    const blocksTemp: number[] = [];
    for (let index = 1; index <= num; index += 1) {
      blocksTemp.push(index);
    }
    return blocksTemp;
  };

  const handleItemClick = (n) => {
    setPageNumberSelect(n);
  };

  useEffect(() => {
    setBlocks(generatePaginationNumbers(numPages));
    if (pageNumberSelect && actualPage !== pageNumberSelect)
      dispatch(userListRequestAction(pageNumberSelect));
  }, [numPages, pageNumberSelect]);

  return (
    <>
      <PaginationContainer>
        {blocks.map((block) => (
          <PaginationItem
            href="#"
            key={block + Math.random()}
            onClick={() => handleItemClick(block)}
          >
            {block}
          </PaginationItem>
        ))}
      </PaginationContainer>
    </>
  );
};

export default Pagination;
