import Users from './user.component';
import { render } from '../../common/test.utils';

describe('User', () => {
  test('renders content', () => {
    const userData = {
      id: 1,
      email: 'asdf@asd.com',
      firstName: 'Juan',
      lastName: 'Perez',
      avatar: 'http://asdf.com',
    };
    const component = render(<Users userData={userData} />);
    component.getByText(userData.firstName);
    component.getByText(userData.lastName);
    component.getAllByAltText('avatar');
  });
});
