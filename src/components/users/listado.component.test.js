import { render } from '../../common/test.utils';
import Listado from './listado.component';

describe('Listado', () => {
  test('renders content', () => {
    const pageData = {
      page: 0,
      perPage: 0,
      total: 0,
      totalPages: 0,
      data: [
        {
          id: 1,
          email: 'asdf@asd.com',
          firstName: 'Juan',
          lastName: 'Perez',
          avatar: 'http://asdf.com',
        },
        {
          id: 2,
          email: 'otrof@asd.com',
          firstName: 'Juanito',
          lastName: 'Pere',
          avatar: 'http://asd.com',
        },
      ],
    };
    const component = render(<Listado pageData={pageData} />);
    // component.getByText('0');
    component.getByText(pageData.data[0].firstName);
    component.getByText(pageData.data[0].lastName);
    component.getByText(pageData.data[0].email);
    component.getByText(pageData.data[1].firstName);
    component.getByText(pageData.data[1].lastName);
    component.getByText(pageData.data[1].email);
    component.getAllByAltText('avatar');
  });
});
