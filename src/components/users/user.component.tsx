import React from 'react';
import {
  AvatarContainer,
  DataContainer,
  UserContainer,
} from '../../styles/model';
import { IUserData } from '../../reducers/users.reducer';

interface IUdata {
  userData: IUserData;
}

const User = ({ userData }: IUdata) => {
  const { email, firstName, lastName, avatar } = userData;
  return (
    <UserContainer>
      <AvatarContainer>
        <img src={avatar} alt="avatar" />
      </AvatarContainer>
      <DataContainer>
        <div>{firstName}</div>
        <div>{lastName}</div>
        <div>{email}</div>
      </DataContainer>
    </UserContainer>
  );
};

export default User;
