import React from 'react';
import { StyledCell, StyledGrid } from '../../styles/model';
import User from './user.component';

interface IPageData {
  pageData: { data: any };
}
const Listado = ({ pageData }: IPageData) => {
  return (
    <StyledGrid>
      {pageData.data.map((user) => (
        <StyledCell key={user.id}>
          <User userData={user} />
        </StyledCell>
      ))}
    </StyledGrid>
  );
};

export default Listado;
