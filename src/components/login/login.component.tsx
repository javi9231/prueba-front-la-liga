import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import {
  Button,
  ErrorMSG,
  Input,
  InputContainer,
  LoginContainer,
  LoginContent,
  Title,
} from '../../styles/model';
import { IRootState } from '../../reducers';
import { loginRequestInAction, isTokenInLocalStorage } from '../../actions';
import ERROR from '../../common/errors.const';

const LoginComponent = () => {
  const initialState = {
    email: '',
    password: '',
  };
  const [data, setData] = useState(initialState);
  const [error, setError] = useState<string>();

  const isLogin =
    useSelector((state: IRootState) => state.auth.isLogin) || false;
  const authError = useSelector((state: IRootState) => state.auth.error) || '';
  useEffect(() => {
    if (authError) {
      setError(ERROR.EMAIL_PASSWORD_ERROR);
    } else {
      setError('');
    }
  }, [authError]);

  const dispatch = useDispatch();
  const onRequestLoginOn = ({ email, password }) =>
    dispatch(loginRequestInAction({ email, password }));

  const handleInputChange = (event: any) => {
    setData({
      ...data,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (data.email && data.password) {
      onRequestLoginOn(data);
    }
  };

  useEffect(() => {
    dispatch(isTokenInLocalStorage());
  }, []);

  const history = useHistory();
  useEffect(() => {
    if (isLogin) {
      history.push('/');
    }
  }, [isLogin]);

  return (
    <LoginContainer>
      <LoginContent>
        <Title>Iniciar sesión con tu Email</Title>
        <form onSubmit={handleSubmit}>
          <InputContainer>
            <Input
              name="email"
              type="email"
              placeholder="Usuario"
              onChange={handleInputChange}
              required
            />
            <Input
              name="password"
              type="password"
              placeholder="Contraseña"
              onChange={handleInputChange}
              required
            />
            <Button type="submit">Iniciar Sesión</Button>
            {error && <ErrorMSG>{error}</ErrorMSG>}
          </InputContainer>
        </form>
      </LoginContent>
    </LoginContainer>
  );
};

export default LoginComponent;
