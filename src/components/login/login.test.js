import { render } from '../../common/test.utils';
import LoginComponent from './login.component';

describe('LoginComponent', () => {
  test('renders content', () => {
    const component = render(<LoginComponent />);
    component.getByText('Iniciar sesión con tu Email');
    component.getByText('Iniciar Sesión');
  });
});
