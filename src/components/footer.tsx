import React from 'react';
import { FooterContainer } from '../styles/model';

const Footer = () => (
  <FooterContainer>Francisco Javier Pastor López</FooterContainer>
);

export default Footer;
