import { render } from '../common/test.utils';
import Header from './header';

describe('HeaderComponent', () => {
  test('renders content', () => {
    const component = render(<Header />);
    component.getByText('Prueba técnica de LaLiga');
  });
});
