import { BaseAction, actionIds } from '../common';

export interface LoginCollectionState {
  isLogin: boolean;
  token: string;
  error: string;
}

const initialState = {
  isLogin: false,
  token: '',
  error: '',
};

const handleGetLoginRequestSuccess = (
  state: LoginCollectionState,
  token: any,
): LoginCollectionState => ({ ...state, token, isLogin: true });

const handleGetLoginRequestError = (
  state: LoginCollectionState,
  error: any,
): LoginCollectionState => ({ ...state, error, isLogin: false, token: '' });

const handleGetLoginRequestOut = (
  state: LoginCollectionState,
): LoginCollectionState => ({ ...state, isLogin: false, token: '' });

export const loginCollectionReducer = (
  state: LoginCollectionState = initialState,
  action: BaseAction,
) => {
  switch (action.type) {
    case actionIds.LOGIN_REQUEST_SUCCESS:
      return handleGetLoginRequestSuccess(state, action.payload);
    case actionIds.LOGIN_REQUEST_ERROR:
      return handleGetLoginRequestError(state, action.payload);
    case actionIds.LOGOUT_REQUEST:
      return handleGetLoginRequestOut(state);
    default:
      break;
  }
  return state;
};
