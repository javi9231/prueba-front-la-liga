import { BaseAction, actionIds } from '../common';

export interface IUserData {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  avatar: string;
}
export interface IUsersListCollectionState {
  data: {
    page: number;
    perPage: number;
    total: number;
    totalPages: number;
    data: IUserData[];
  };
  error: string;
  isLoading: boolean;
}

const initialState = {
  data: {
    page: 0,
    perPage: 0,
    total: 0,
    totalPages: 0,
    data: [],
  },
  error: '',
  isLoading: false,
};
// eslint da un error con la nomenclatura xx_xx en las variable
// por lo que se opta por un mapper
const mapperPageData = (listPage: any): IUsersListCollectionState => ({
  data: {
    page: listPage.page,
    perPage: listPage.per_page,
    total: listPage.total,
    totalPages: listPage.total_pages,
    data: listPage.data.map((dat) => ({
      id: dat.id,
      email: dat.email,
      firstName: dat.first_name,
      lastName: dat.last_name,
      avatar: dat.avatar,
    })),
  },
  error: '',
  isLoading: false,
});

const handleUserListRequestSuccess = (
  state: IUsersListCollectionState,
  page: any,
): IUsersListCollectionState => {
  const mapperListData = mapperPageData(page);
  return { ...state, ...mapperListData };
};

const handleUserListRequestError = (
  state: IUsersListCollectionState,
  error: any,
): IUsersListCollectionState => ({ ...state, error });

const handleUserListRequest = (
  state: IUsersListCollectionState,
): IUsersListCollectionState => ({ ...state, isLoading: true });

export const usersCollectionReducer = (
  state: IUsersListCollectionState = initialState,
  action: BaseAction,
) => {
  switch (action.type) {
    case actionIds.USER_LIST_REQUEST:
      return handleUserListRequest(state);
    case actionIds.USER_LIST_SUCCESS:
      return handleUserListRequestSuccess(state, action.payload);
    case actionIds.USER_LIST_ERROR:
      return handleUserListRequestError(state, action.payload);
    default:
      break;
  }
  return state;
};
