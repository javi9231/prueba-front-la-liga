import { combineReducers } from 'redux';
import { loginCollectionReducer, LoginCollectionState } from './login.reducer';
import {
  usersCollectionReducer,
  IUsersListCollectionState,
} from './users.reducer';

export interface IRootState {
  auth: LoginCollectionState;
  users: IUsersListCollectionState;
}

export const rootReducers = combineReducers<IRootState>({
  auth: loginCollectionReducer,
  users: usersCollectionReducer,
});
